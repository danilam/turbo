<?php
/**
 * Project turbo
 * Created by danila 07.08.19 @ 20:50
 */
ini_set("max_execution_time", 0);
require_once("./vendor/autoload.php");
$core = new \Core\Base\BaseCore();
$queue = $core->getQueueManager("Dummy");

for (; ;) {
    if (!$task = $queue->getNewTask()) {
        //Sleep for 5 seconds
        sleep(5);
    } else {
        foreach ($task->methods as $method) {
            $task->text = $core->getParser($method)->prepareText($task->text);
        }
        $queue->taskDone($task->id, $task->text);
    }
}
