<?php
/**
 * Project turbo
 * Created by danila 07.08.19 @ 21:44
 */

namespace Tests;

use Core\Parsers;

class BaseParserTest extends \PHPUnit\Framework\TestCase
{

    public function test_stripTags()
    {
        $input = "Привет <a href=\"link\">ссылка</a> медвед";
        $task = (new Parsers\Parser_stripTags())->prepareText($input);
        return $this->assertEquals("Привет ссылка медвед", $task, "test_stripTags");
    }

    public function test_removeSpaces()
    {
        $input = "Привет <a href=\"link\">ссылка</a> медвед";
        $task = (new Parsers\Parser_removeSpaces())->prepareText($input);
        return $this->assertEquals("Привет<ahref=\"link\">ссылка</a>медвед", $task, "test_removeSpaces");
    }

    public function test_replaceSpacesToEol()
    {
        $input = "Привет медвед ";
        $task = (new Parsers\Parser_replaceSpacesToEol())->prepareText($input);
        return $this->assertEquals("Привет\nмедвед\n", $task, "test_replaceSpacesToEol");
    }

    public function test_htmlspecialchars()
    {
        $input = "Привет&медвед<>";
        $task = (new Parsers\Parser_htmlspecialchars())->prepareText($input);
        return $this->assertEquals("Привет&amp;медвед&lt;&gt;", $task, "test_htmlspecialchars");
    }

    public function test_removeSymbols()
    {
        $input = "Привет&медвед<>[.,/!@#\$%^&*()]";
        $task = (new Parsers\Parser_removeSymbols())->prepareText($input);
        return $this->assertEquals("Приветмедвед<>", $task, "test_removeSymbols");
    }

    public function test_toNumber()
    {
        $input = "Привет&мед11вед<>[.,/!@#\$%^&*()]22";
        $task = (new Parsers\Parser_toNumber())->prepareText($input);
        return $this->assertEquals("11", $task, "test_toNumber");
    }

}
