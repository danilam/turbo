<?php
/**
 * Project turbo
 * Created by danila 07.08.19 @ 21:12
 */

namespace Core\Base;


class BaseTask
{
    var $id = 0;
    var $text = "";
    var $methods = [];

    function __construct($id, $data)
    {
        $this->id = $id;
        $this->text = $data["text"];
        $this->methods = $data["methods"];
    }
}