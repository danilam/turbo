<?php
/**
 * Project turbo
 * Created by danila 07.08.19 @ 20:57
 */

namespace Core\Base;


abstract class BaseParser
{
    /**
     * @param $text string
     * @return mixed
     */
    abstract public function prepareText($text);
}