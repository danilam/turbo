<?php
/**
 * Project turbo
 * Created by danila 07.08.19 @ 20:55
 */

namespace Core\Base;


abstract class BaseQueue
{
    /**
     * @return BaseTask|null
     */
    abstract public function getNewTask();

    /**
     * @param $taskID int
     * @param $result string
     * @return bool
     */
    abstract public function taskDone($taskID, $result);

    /**
     * @param $taskID int
     * @return bool
     */
    abstract public function taskError($taskID);

}