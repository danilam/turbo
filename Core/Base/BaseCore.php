<?php
/**
 * Project turbo
 * Created by danila 07.08.19 @ 21:04
 */

namespace Core\Base;


class BaseCore
{
    /**
     * @param $type
     * @return BaseParser
     * @throws \Exception
     */
    public function getParser($type)
    {
        $className = "Core\\Parsers\\Parser_" . $type;
        if (!class_exists($className)) {
            throw new \Exception("Parser " . $type . " not found");
        }

        return new $className();
    }

    /**
     * @param $type
     * @return BaseQueue
     * @throws \Exception
     */
    public function getQueueManager($type)
    {
        $className = "Core\\Queue\\Queue" . $type;
        if (!class_exists($className)) {
            throw new \Exception("Queue manager " . $type . " not found");
        }

        return new $className();

    }

}