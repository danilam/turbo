<?php
/**
 * Project turbo
 * Created by danila 07.08.19 @ 21:27
 */

namespace Core\Parsers;


use Core\Base\BaseParser;
use Core\Base\BaseTask;

class Parser_removeSymbols extends BaseParser
{
    /**
     * @param $text string
     * @return mixed
     */
    public function prepareText($text)
    {
        return preg_replace("#[\\[\\].,/!@\\#$%^&*()]#s","", $text);
    }


}