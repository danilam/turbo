<?php
/**
 * Project turbo
 * Created by danila 07.08.19 @ 21:27
 */

namespace Core\Parsers;


use Core\Base\BaseParser;
use Core\Base\BaseTask;

class Parser_stripTags extends BaseParser
{
    /**
     * @param $text string
     * @return mixed
     */

    public function prepareText($text)
    {
        return strip_tags($text);
    }


}