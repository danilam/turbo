<?php
/**
 * Project turbo
 * Created by danila 07.08.19 @ 21:27
 */

namespace Core\Parsers;


use Core\Base\BaseParser;
use Core\Base\BaseTask;

class Parser_toNumber extends BaseParser
{
    /**
     * @param $text string
     * @return mixed
     */
    public function prepareText($text)
    {
        if (preg_match("#([0-9]+)#s", $text, $preg)) {
            return $preg[1];
        } else {
            return 0;
        }

    }


}