<?php
/**
 * Project turbo
 * Created by danila 07.08.19 @ 21:23
 */

namespace Core\Queue;


use Core\Base\BaseQueue;
use Core\Base\BaseTask;

class QueueDummy extends BaseQueue
{
    /**
     * @return BaseTask|void|null
     */
    public function getNewTask()
    {
        $id = 1;
        $task = json_decode('{"job": {"text": "Привет, мне на <a href=\"test@test.ru\">test@test.ru</a> пришло приглашение встретиться, попить кофе с <strong>10%</strong> содержанием молока за <i>$5</i>, пойдем вместе!","methods": ["stripTags", "removeSpaces", "replaceSpacesToEol", "htmlspecialchars", "removeSymbols", "toNumber"]}}', true);

        return new BaseTask($id, $task["job"]);
    }

    public function taskDone($taskID, $result)
    {
        echo json_encode(["text"=>$result]);
    }

    public function taskError($taskID)
    {
        // TODO: Implement taskError() method.
    }


}